module.exports = {
	root: true,
	env: {
		node: true,
		webextensions: true,
	},
	extends: [
		'plugin:vue/essential',
		'@vue/airbnb',
	],
	parserOptions: {
		parser: 'babel-eslint',
	},
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'import/extensions': ['error', 'always', {
			js: 'never',
			vue: 'never'
		}],
		'import/no-extraneous-dependencies': ['error', {
			devDependencies: true,
		}],
		"no-tabs": 0,
		"quotes": 0,
		"indent": ["error", "tab", { "ignoreComments": true }],
		"radix": ["error", "as-needed"],
		'max-len': ['error', 100, 1, {
			ignoreUrls: true,
			ignoreComments: false,
			ignoreRegExpLiterals: true,
			ignoreStrings: true,
			ignoreTemplateLiterals: true,
		}],
		'object-curly-newline': ['error', {
			ObjectExpression: { minProperties: 5, multiline: true, consistent: true },
			ObjectPattern: { minProperties: 5, multiline: true, consistent: true },
			ImportDeclaration: { minProperties: 5, multiline: true, consistent: true },
			ExportDeclaration: { minProperties: 5, multiline: true, consistent: true },
		}],
		'comma-dangle': ['error', {
			arrays: 'always-multiline',
			objects: 'always-multiline',
			imports: 'always-multiline',
			exports: 'always-multiline',
			functions: 'never',
		}],
	},
};

