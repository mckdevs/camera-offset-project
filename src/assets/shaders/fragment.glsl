
vec3 rgb(float r, float g, float b) {
    return vec3(r / 255., g / 255., b / 255.);
}

vec3 rgb(float c) {
    return vec3(c / 255., c / 255., c / 255.);
}

float cubicPulse( float c, float w, float x ){
	x = abs(x - c);
	if( x>w ) return 0.0;
	x /= w;
	return 1.0 - x*x*(3.0-2.0*x);
}

uniform vec3 u_bg;
uniform vec3 u_bgMain;
uniform vec3 u_color1;
uniform vec3 u_color2;
uniform float u_time;

varying vec2 vUv;
varying float vDistortion;

void main() {
    vec3 bg = rgb(u_bg.r, u_bg.g, u_bg.b);
    vec3 c1 = rgb(u_color1.r, u_color1.g, u_color1.b);
    vec3 c2 = rgb(u_color2.r, u_color2.g, u_color2.b);
    vec3 bgMain = rgb(u_bgMain.r, u_bgMain.g, u_bgMain.b);

    float noise1 = snoise(vUv + u_time * 0.08);
    float noise2 = snoise(vUv * 2. + u_time * 0.1);

    vec3 color = bg;
    color = mix(color, c1, noise1 * 0.9);
    color = mix(color, c2, noise2 * 0.9);

    color = mix(color, mix(c1, c2, vUv.x), vDistortion);

    float border = cubicPulse(0.545, 0.550, vUv.x);

    color = mix(color, bgMain, border);

    gl_FragColor = vec4(color, 1.0);
}