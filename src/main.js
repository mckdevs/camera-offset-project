import Vue from 'vue';
import Vuex from 'vuex';
import VueAgile from 'vue-agile';
import App from './App';
import 'sal.js/dist/sal.css';

Vue.config.productionTip = false;

Vue.use(
	VueAgile,
	Vuex
);

new Vue({
	render: (h) => h(App),
}).$mount('#app');
